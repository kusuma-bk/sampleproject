package com.org.flightbooking.service;

import com.org.flightbooking.dto.UserDto;
import com.org.flightbooking.exception.CustomException;
import com.org.flightbooking.exception.SuccessLoginException;
import com.org.flightbooking.repository.LoginException;

public interface UserService {
	public String checkLoginByUserId(String emailId, String password) throws CustomException, LoginException,SuccessLoginException;

}
